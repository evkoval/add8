#pragma once
#include <iostream>

namespace ek
{
	struct Point 
	{ 
		float x; float y; 
	};

	class Circle
	{

	public:
		Circle();

		Circle(Point o, float r);

		~Circle();

		float Square();

		float Dlina();

		void SetO(Point o);

		void SetR(float r);

	private:
		Point m_o;
		float m_r;
	};
}