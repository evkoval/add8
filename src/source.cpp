#include <header.hpp>

namespace ek
{
	float pi = acos(-1);

	Circle::Circle()
	{
	}


	Circle::Circle(Point o, float r)
	{
		m_r = r;
		m_o.x = o.x;
		m_o.y = o.y;
		if (m_r <= 0)
		{
			std::cout << "������ r <= 0" << std::endl;
			m_r = 1;
		}
	}

	Circle::~Circle()
	{
	}

	float Circle::Square()
	{
		return m_r * m_r * pi;
	}

	float Circle::Dlina()
	{
		return pi * m_r * 2;
	}

	void Circle::SetO(Point o)
	{
		m_o.x = o.x;
		m_o.y = o.y;
	}

	void Circle::SetR(float r)
	{
		m_r = r;
		if (m_r <= 0)
		{
			std::cout << "������ r <= 0" << std::endl;
			m_r = 1;
		}
	}
}